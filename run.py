import requests
from bs4 import BeautifulSoup
from json import dump
import wget

def save_response():
    url="https://www.thebiomics.com/notes/springer-free-e-books-list.html"
    headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36',
    }

    response=requests.get(url,headers=headers)
    with open("index.html",'w') as file:
        file.write(response.text)

def read_html():
    loop_count=1
    loop_limit=2

    while loop_count <= loop_limit:
        try:
            with open("index.html",'r') as file:
                site=file.read()
        except Exception as e:
            print(e)
            save_response()
        else:
            return site

        finally:
            loop_count += 1

def save_books(books):
    with open('springer_books.json', 'w') as fp:
        dump(books, fp, ensure_ascii=False, indent=2)

def download_book_as_pdf(url):
    try:
        filename=wget.download(url)
    except Exception as e:
        print(e)
        return None
    else:
        return filename

def download_books(books, start=0, end=None):
    downloaded_books=[]
    failed_books=[]

    if end is None: end=len(books)
    for book in books[start:end]:
        book_title=book.get('title')
        print(book_title)
        filename=download_book_as_pdf(book.get("download_url"))
        if filename is not None:
            downloaded_books.append(book_title)
        else: failed_books.append(book_title)

    return downloaded_books,failed_books

def parse_book_html():
    downloadable_books=[]
    soup = BeautifulSoup(read_html(),"lxml")
    books=soup.find("tbody").find_all("tr")
    for book in books:
        book_title=book.find("td").a.text
        book_detail_url=book.find("td").a.get("href")
        book_download_url=book.find("a",class_="download-link link-external link-js link m-0").get("data-href")
        downloadable_books.append(
            {
            'title':book_title,
            'detail_url':book_detail_url,
            'download_url':book_download_url
            }
        )

    return downloadable_books

def main():
    books=parse_book_html()
    save_books(books)
    downloaded,failed=download_books(books)
    print(f"\n{len(downloaded)} books downloaded")

if __name__=="__main__":
    main()
